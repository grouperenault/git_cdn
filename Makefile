.PHONY: build

MODULE:=git_cdn config.py
PYTHON?=python3
POETRY?=poetry
PIPX?=pipx

POETRY_VERSION:=$(shell cat .poetry-version | grep -v '\#')
PYTHON_VERSION:=$(shell cat .python-version | grep -v '\#')

# poetry enforce semver PEP 440 https://www.python.org/dev/peps/pep-0440/#local-version-identifiers
# So convert v1.7.1-55-af3454 to v1.7.1+55.af3454
GITCDN_VERSION := $$(git describe --tags HEAD | sed s/\-/\+/ | sed s/\-/\./)
GITCDN_LOCALCHANGE := $$(if [ "$$(git status -s -uno)" ]; then echo ".dirty"; fi)

all: dev style checks test
style: isort black
dev: ensure-poetry ensure-python check-versions poetry-install  ## Setup dev environment
dev-no-check: ensure-poetry ensure-python poetry-install  ## Setup dev environment without checking python and poetry versions


sc: style checks
sct: style checks test
checks: isort-check black-check flake8 pylint

git-config:
	git config --global uploadpack.allowfilter true

ensure-python:
	$(POETRY) env use $$(which $(PYTHON))

ensure-poetry:
	@if [ "$$IN_NIX_SHELL" ]; then \
		echo "Let nix makes python version $(PYTHON_VERSION) and poetry version $(POETRY_VERSION) available"; \
	else \
		if ! command -v $(POETRY) &> /dev/null || ! $(POETRY) --version |grep -q $(POETRY_VERSION); then \
			$(PIPX) install --force "poetry==$(POETRY_VERSION)"; \
		fi; \
		if ! $(POETRY) self show plugins | grep -q poetry-plugin-export; then \
			$(PIPX) inject --force poetry poetry-plugin-export; \
		fi; \
	fi

poetry-install:
	@$(POETRY) install
	@if [ "$$IN_NIX_SHELL" ]; then \
		scripts/patch_libs.sh; \
	fi

# use on nix develop shell (@poetry, python versions managed by nix)
check-versions:
	if $(POETRY) --version |grep -q $(POETRY_VERSION); then \
		echo Right poetry version: $(POETRY_VERSION); \
	else \
		echo "Bad poetry version: current: $(shell $(POETRY) --version) expected: $(POETRY_VERSION)"; \
		echo $(DONT_FORGET_NIX); \
		exit 1; \
	fi; \
	if $(POETRY) run $(PYTHON) --version |grep -q $(PYTHON_VERSION); then \
		echo Right python version: $(PYTHON_VERSION) ;\
	else \
		echo "Bad python version:";\
		echo "    current on venv: $(shell $(POETRY) run $(PYTHON) --version)" ;\
		echo "    current on system: $(shell $(PYTHON) --version)" ;\
		echo "    expected: $(PYTHON_VERSION)" ;\
		echo $(DONT_FORGET_NIX) ;\
		exit 1; \
	fi; \

export:
	@$(POETRY) export --without-hashes --output requirements.txt

build:
	@$(POETRY) build

publish:
	@$(POETRY) publish

install-no-dev:
	@$(POETRY) install --no-dev

isort:
	@$(POETRY) run isort $(MODULE)

isort-check:
	@$(POETRY) run isort -c $(MODULE)

black:
	@$(POETRY) run black $(MODULE)

black-check:
	@$(POETRY) run black --check $(MODULE)

flake8:
	$(POETRY) run flake8 --config .flake8 $(MODULE)

pylint:
	$(POETRY) run pylint --rcfile=.pylint.toml --output-format=colorized $(MODULE)

set-version:
	$(POETRY) version $(GITCDN_VERSION)$(GITCDN_LOCALCHANGE)

test: git-config
	@$(POETRY) run pytest --strict $(MODULE)

integration-test: git-config
	@$(POETRY) run pytest --strict git_cdn/tests/test_integ.py

test-v: git-config
	@$(POETRY) run pytest --strict -svv --durations=30 $(MODULE)

test-coverage: git-config
	@$(POETRY) run pytest --junitxml=testresults.xml -v --cov --cov-report term-missing --cov-report html:`pwd`/coverage_html  $(MODULE)

docker:
	docker build .

ctags:
	find -name '*.py' -exec ctags -a {} \;

update:
	@$(POETRY) update

lock:
	@$(POETRY) lock

githook: style

push: githook
	git push origin --all
	git push origin --tags

run: git-config
	. ./tosource && \
	echo $$LOGGING_SERVER|grep 127.0.0.1 && docker compose up -d; \
	$(POETRY) run gunicorn -c config.py git_cdn.app:app -b :8000

run-uvloop: git-config
	. ./tosource && \
	echo $$LOGGING_SERVER|grep 127.0.0.1 && docker compose up -d; \
	GUNICORN_WORKER_CLASS=aiohttp.worker.GunicornUVLoopWebWorker \
	$(POETRY) run gunicorn -c config.py git_cdn.app:app -b :8000

# aliases to gracefully handle typos on poor dev's terminal
check: checks
devel: dev
develop: dev
styles: style
test-unit: test
unit-tests: test
ut: test

# ==================================================================================================
# Clean targets
# ==================================================================================================


clean-dist:
	rm -rfv build dist/

clean-mypy:
	rm -rf .mypy_cache || true

clean-venv:
	@rm -fr .venv

clean: clean-dist clean-mypy clean-venv  ## Clean environment
	find . -name '__pycache__'  -exec rm -rf {} \; || true
	find . -name '.cache'  -exec rm -rf {} \; || true
	find . -name '*.egg-info'  -exec rm -rf {} \; || true
	find . -name "*.pyc" -exec rm -f {} \; || true
	rm -rf .pytest_cache || true
	rm -rf coverage_html || true

