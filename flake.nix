{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  outputs =
    {
      self,
      nixpkgs,
    }:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};

      # For devShell:
      libs = with pkgs; [
        glibc
        gcc.cc
        zlib
      ];
      libs_path = pkgs.lib.strings.makeLibraryPath libs;
    in
    {
      formatter.${system} = pkgs.nixfmt-rfc-style;
      devShells.${system}.default = pkgs.mkShellNoCC {
        packages = with pkgs; [
          python312
          poetry
          glibc # for ldd in patch_libs
          patchelf # for patch_libs
        ];
        POETRY = "poetry";
        PYTHON_KEYRING_BACKEND = "keyring.backends.fail.Keyring";
        # Used on scripts/patch_libs.sh
        LD_LIBRARY_PATH_BASE = libs_path;
        buildInputs = [ pkgs.bashInteractive ];
        nativeBuildInputs = with pkgs; [ makeWrapper ];
        shellHook = ''
          echo "
          You can now execute:
          $ make nixdev
          $ poetry install
          $ patch_libs.sh
          $ poetry run ...
          "
          echo "$ python --version"
          python --version
          echo "$ poetry --version"
          poetry --version
        '';
      };
    };
}
