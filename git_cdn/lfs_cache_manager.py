import asyncio
import fcntl
import json
import os
import time
from urllib.parse import urlparse

from aiohttp import web
from aiohttp.web_exceptions import HTTPNotFound
from structlog import getLogger
from structlog.contextvars import bind_contextvars

from git_cdn.client_session import ClientSessionWithRetry
from git_cdn.lock.aio_lock import lock
from git_cdn.util import check_path
from git_cdn.util import get_subdir

log = getLogger()


class LFSCacheFile:
    def __init__(self, href, headers):
        self.href = href
        self.headers = headers
        self.accept_encoding = ""
        if "Accept-Encoding" in headers:
            self.accept_encoding = headers["Accept-Encoding"]
        path = urlparse(href).path.lstrip("/")
        check_path(path)
        self.workdir = get_subdir("lfs")
        self.hash = os.path.basename(path)
        self.filename = os.path.join(
            self.workdir, os.path.dirname(path), self.hash[:2], self.hash
        )

    @property
    def gzip(self):
        return f"{self.filename}.gzip"

    def read_lock(self):
        return lock(self.filename, mode=fcntl.LOCK_SH)

    def write_lock(self):
        return lock(self.filename, mode=fcntl.LOCK_EX)

    @staticmethod
    def delete(files):
        if isinstance(files, str):
            files = [files]
        for file in files:
            if os.path.exists(file):
                os.unlink(file)

    def gzip_exists(self):
        return os.path.exists(self.gzip) and os.stat(self.gzip).st_size > 0

    def raw_exists(self):
        return os.path.exists(self.filename) and os.stat(self.filename).st_size > 0

    def exists(self):
        return (
            "gzip" in self.accept_encoding and self.gzip_in_cache() or self.raw_exists()
        )

    def utime(self):
        os.utime(self.filename, None)

    def gzip_utime(self):
        os.utime(self.gzip, None)

    @staticmethod
    async def gunzip(filename):
        # We do not want to compute the gunzip using python stlib
        # - file may be huge, native gunzip will have better perf
        # - avoid python memory allocations and GC operations
        p = await asyncio.create_subprocess_exec(
            "gunzip",
            "-f",
            "-k",
            "-S",
            ".gzip",
            filename,
            stderr=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
        )
        cmd_stdout, cmd_stderr = await p.communicate()
        r = p.returncode
        if r:
            log.error(
                "gunzip failed",
                cmd_stderr=cmd_stderr.decode(),
                cmd_stdout=cmd_stdout.decode(),
                lfs_gzip=filename,
                gunzip_pid=p.pid,
            )
        return r

    @staticmethod
    async def checksum(filename, filehash):
        # We do not want to compute the sha using python stlib
        # - file may be huge, native checksum will have better perf
        # - avoid python memory allocations and GC operations
        p = await asyncio.create_subprocess_exec(
            "sha256sum",
            filename,
            "-b",
            stdin=asyncio.subprocess.PIPE,
            stdout=asyncio.subprocess.PIPE,
        )
        cmd_stdout, cmd_stderr = await p.communicate()
        cs = cmd_stdout.decode().split(" ")[0]
        if cs != filehash:
            log.error(
                "bad checksum",
                cmd_stderr=cmd_stderr.decode() if cmd_stderr else "",
                cmd_stdout=cmd_stdout.decode(),
                lfs_filename=filename,
                lfs_expected_checksum=filehash,
                lfs_actual_checksum=cs,
                checksum_pid=p.pid,
            )
        return cs == filehash

    def gzip_in_cache(self):
        if self.gzip_exists():
            self.gzip_utime()
            return True
        return False

    def raw_in_cache(self):
        if self.raw_exists():
            self.utime()
            return True
        return False

    def response(self):
        if "gzip" in self.accept_encoding and self.gzip_in_cache():
            bind_contextvars(lfs_served="gzip")
            return web.Response(
                body=open(self.gzip, "rb"),
                headers={
                    "Content-Encoding": "gzip",
                    "Content-Length": str(os.stat(self.gzip).st_size),
                },
            )
        if self.raw_in_cache():
            bind_contextvars(lfs_served="raw")
            return web.Response(
                body=open(self.filename, "rb"),
                headers={
                    "Content-Length": str(os.stat(self.filename).st_size),
                },
            )
        return None

    async def download(self, get_session, ctx):
        t1 = time.time()
        async with ClientSessionWithRetry(
            get_session,
            range(500, 600),
            "get",
            self.href,
            headers=self.headers,
            allow_redirects=False,
        ) as response:
            if "Content-Length" in response.headers:
                ctx["lfs_content_length"] = response.headers["Content-Length"]
            if response.status != 200:
                raise HTTPNotFound(body=await response.content.read())

            gzpartfile = ""
            partfile = f"{self.filename}.part"
            if (
                "Content-Encoding" in response.headers
                and "gzip" in response.headers["Content-Encoding"]
            ):
                ctx["lfs_content_encoding"] = "gzip"
                gzpartfile = f"{self.filename}.part.gzip"
                target = gzpartfile
            else:
                target = partfile
            with open(target, "wb") as f:
                try:
                    while chunk := await response.content.readany():
                        f.write(chunk)

                except Exception:
                    # don't need to raise, if the file is not present, we will try again
                    log.exception("Aborting lfs download", **ctx)
                    self.delete(target)
                    return

            t2 = time.time()
            ctx["lfs_download_duration"] = t2 - t1
            if gzpartfile:
                await self.gunzip(gzpartfile)
                ctx["lfs_uncompress_duration"] = time.time() - t2

            if not await self.checksum(partfile, self.hash):
                self.delete([target, gzpartfile])
                return

            if gzpartfile:
                os.rename(gzpartfile, self.gzip)
            os.rename(partfile, self.filename)


class LFSCacheManager:
    """Unit testable LFS cache manager which download LFS objects in parallel

    Design Model: to workaround per connection limitation of the proxy, we download LFS objects in
    parallel.
    We still limit the number of connection via aiohttp client connection pool.

    Because the LFS client might only download a smaller number of objects in parallel,
    we start the parallel download of all the files of a batch as soon as we get the batch
    attempt from the batch API.

    We use aiolock writeonce, read multiple in order to make sure we only download the same object
    once.
    """

    def __init__(self, upstream_url, base_url, get_session):
        self.upstream_url = upstream_url
        self.base_url = base_url
        self.get_session = get_session
        self.ctx = {}

    def set_base_url(self, base_url):
        self.base_url = base_url

    async def hook_lfs_batch(self, lfs_result_content):
        """modify the lfs batch result to change the hrefs so that they point to us"""
        js = json.loads(lfs_result_content)
        if "objects" not in js:
            return lfs_result_content
        for o in js["objects"]:
            if "actions" not in o:
                continue

            for action in ["download", "upload", "verify"]:
                if action in o["actions"]:
                    href = o["actions"][action]
                    href["href"] = href["href"].replace(
                        self.upstream_url, self.base_url
                    )

        return json.dumps(js).encode()

    async def get_from_cache(self, href, headers):
        """@returns: filename where to find the file
        raises: HTTPNotFound in case of impossibility to download the file
        """
        if self.base_url is not None and href.startswith(self.base_url):
            href = href.replace(self.base_url, self.upstream_url)

        cache_file = LFSCacheFile(href, headers)
        self.ctx = {"lfs_href": href, "lfs_content_encoding": "none"}

        async with cache_file.read_lock():
            r = cache_file.response()
            if r is not None:
                bind_contextvars(lfs_hit=True, **self.ctx)
                return r

        async with cache_file.write_lock():
            r = cache_file.response()
            if r is not None:
                bind_contextvars(lfs_hit=True, **self.ctx)
                return r
            bind_contextvars(lfs_hit=False, **self.ctx)
            retries = 0
            while not cache_file.exists():
                bind_contextvars(download_retries=retries)
                if retries > 2:
                    raise HTTPNotFound(body="failed to get LFS file")
                await asyncio.shield(cache_file.download(self.get_session, self.ctx))
                retries += 1

            return cache_file.response()
